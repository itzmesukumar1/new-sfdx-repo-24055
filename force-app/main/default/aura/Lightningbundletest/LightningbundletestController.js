({
	 clearRecord: function(component, event){ 
        component.set("v.wrapperList", []);
        var casetest = component.get('v.wrapperList');
        var rowsize =  casetest.length - 1;
        component.set("v.wrapperListsize", rowsize);
        var RowItemList = component.get("v.dccOnwerDetails");
        RowItemList ={'cwsId':'' ,'firstName':'','lastName':'','email':'',
                      'subscription':'','dealerCode':'',
                      'address1':'','address2':'','city':'','country':'','state':'','zipcode':'',
                      'isfirstName':false,'iscwsId':false,'islastName':false,'isemail': false,
                      'issubscription':false,'isdealerCode':false,'isconCwsID':false,'isconFirstName':false,'isconLastName':false,'isconEmail':false,
                      'isaddress1':false,'isaddress2':false,'iscity':false,'iscountry':false,'isstate':false,'iszipcode':false,'iscwsID':false,'isfirstName':false,'islastName':false
                     };
        component.set("v.dccOnwerDetails", RowItemList);
        component.set("v.isErrorMsg", false);
        component.set("v.onchangeErr", false);
    },
})